import React, { ChangeEvent, useEffect, useState } from 'react';
import { HubConnection, HubConnectionBuilder, HubConnectionState } from '@microsoft/signalr';
import SignalRow from './SignalRow';
import '../Table.module.css';

interface DataPoint {
    X: number,
    Y: number
}

const SignalDisplay = () => {
    const [connection, setConnection] = useState<null | HubConnection>(null);
    const [signals, setSignals] = useState<string[]>([]);
    const [selectedSignal, setSelectedSignal] = useState<string>("");
    const [observedSignals, setObservedSignals] = useState<string[]>([]);
    const [signalData, setSignalData] = useState<{ [key: string]: DataPoint[] }>({});

    useEffect(() => {
        const newConnection = new HubConnectionBuilder().withUrl('http://localhost:5233/hubs/data').withAutomaticReconnect().build();
        setConnection(newConnection);

        fetch("http://localhost:5233/signals/setsignals").then((res) => res.json()).then((signals: string[]) => {
            setObservedSignals(signals);
        });

        fetch("http://localhost:5233/signals/startrecording", { method: "POST" });

        fetch("http://localhost:5233/signals").then((res) => res.json()).then((signals: string[]) => {
            setSignals(signals);
        });
    }, []);

    useEffect(() => {
        if (connection && connection.state === HubConnectionState.Disconnected) {
            connection.start().then(_ => {
                console.log("Connected!");
                connection.on("SendDataToClients", message => {
                    let new_data = JSON.parse(message);
                    setSignalData(new_data);
                });
            }).catch(e => console.log(e));
        }
    }, [connection]);

    const onChange = (e: ChangeEvent<HTMLSelectElement>) => {
        setSelectedSignal(e.target.value);
    }

    const addRow = () => {
        if (signals.includes(selectedSignal) && !observedSignals.includes(selectedSignal)) {
            fetch("http://localhost:5233/signals/setsignals", {
                method: "POST",
                body: JSON.stringify([...observedSignals, selectedSignal]),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((_) => {
                setObservedSignals((observedSignals) => [...observedSignals, selectedSignal]);
            });
        }
    }

    const removeRow = (signalToRemove: string) => {
        const newSignals = observedSignals.filter((signal) => signal !== signalToRemove);
        fetch("http://localhost:5233/signals/setsignals", {
            method: "POST",
            body: JSON.stringify(newSignals),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((_) => {
            setObservedSignals(newSignals);
        });
    }

    const getDataValue = (signal: string) => {
        if (signalData[signal]) {
            return signalData[signal].at(-1)?.Y || -1;
        }
        return -1;
    }

    return (
        <div>
            <div>
                <select value={selectedSignal} onChange={onChange}>
                    <option hidden>Select Signal</option>
                    {signals.filter((signal) => !observedSignals.includes(signal)).map((signal) => <option key={signal}>{signal}</option>)}
                </select>
                <button onClick={addRow}>Add</button>
            </div>
            <table style={{ border: "1px solid" }}>
                <thead>
                    <tr>
                        <th>Signal Name</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    {observedSignals.map((signal, idx) => <SignalRow key={idx} signalName={signal} signalValue={getDataValue(signal)} removeRow={removeRow} />)}
                </tbody>
            </table>
        </div>
    )
}

export default SignalDisplay;