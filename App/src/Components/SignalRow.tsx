import React from 'react';
import '../Table.module.css';

interface ISignalRowProps {
    removeRow: Function,
    signalName: string,
    signalValue: number
}

const SignalRow = (props: ISignalRowProps) => {
    const handleRemoval = (e: React.MouseEvent<HTMLElement>) => {
        e.preventDefault();
        console.log(`Removing ${props.signalName}`);
        props.removeRow(props.signalName);
    }

    console.log(props.signalValue);
    

    return (
        <tr>
            <td>
                {props.signalName}
            </td>
            <td>
                {props.signalValue.toFixed(2)}
            </td>
            <td>
                <button onClick={handleRemoval}>X</button>
            </td>
        </tr>
    );
}

export default SignalRow;