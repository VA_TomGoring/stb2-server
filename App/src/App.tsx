import React from 'react';
import './App.css';
import SignalDisplay from './Components/SignalDisplay';

function App() {
  return (
    <div className="App">
      <SignalDisplay/>
    </div>
  );
}

export default App;
