namespace Models;

public record DataPoint
{
    public double X { get; }
    public double Y { get; }

    public DataPoint(double x, double y)
    {
        X = x;
        Y = y;
    }
}