using Interfaces;
using Models;

namespace Testing;

public enum SimState
{
    Initialised,
    Started,
    Stopped,
}

public class TestSim : ISim
{
    public SimState State { get; set; }
    private Dictionary<string, dynamic> Parameters { get; }
    private IList<string> Signals { get; }
    private IList<string> _SelectedSignals;

    public IList<string> SelectedSignals
    {
        get => _SelectedSignals;
        set
        {
            Console.WriteLine("Setting signals...");
            if (value.All((signal) => Signals.ToList().Contains(signal)))
            {
                _SelectedSignals = value;
                Console.WriteLine(string.Join(", ", SelectedSignals));
            }
            else
            {
                var invalidSignals = string.Join(", ", value.Where((signal) => !Signals.ToList().Contains(signal)));
                throw new Exception($"Attempted to set invalid signals to record: {invalidSignals}");
            }
        }
    }
    private double counter;
    private bool recording;

    public TestSim()
    {
        State = SimState.Initialised;
        Parameters = new Dictionary<string, dynamic>
        {
            { "test_param:1", 1 }
        };

        Signals = new List<string>
        {
            { "test_signal1:1"},
            { "test_signal2:1"},
            { "test_signal3:1"},
            { "test_signal4:1"}
        };

        _SelectedSignals = new List<string>();
    }

    public Dictionary<string, IList<DataPoint>> FetchRecordedData()
    {
        var fakeData = new Dictionary<string, IList<DataPoint>>();
        if (recording)
        {
            foreach (var signal in SelectedSignals)
            {
                fakeData.Add(signal, new List<DataPoint>());
                for (int i = 0; i < 100; i++)
                {
                    counter += 0.01;
                    var data = new DataPoint(counter, Math.Sin(counter + (int)GetParam("test_param:1")));//(counter, Math.Sin(counter + (int)GetParam("test_param:1")));
                    fakeData[signal].Add(data);
                }
            }
        }
        return fakeData;
    }

    public IList<string> GetParameterNames()
    {
        return Parameters.Keys.ToList();
    }

    public dynamic GetParam(string param)
    {
        return Parameters[param];
    }

    public void SetParam(string param, dynamic raw_value)
    {
        Parameters[param] = raw_value;
    }

    public void Start()
    {
        State = SimState.Started;
    }

    public void Stop()
    {
        State = SimState.Stopped;
        counter = 0.0;
    }

    public IList<string> GetSignalNames()
    {
        return Signals.ToList();
    }

    public void StartRecording()
    {
        recording = true;
    }

    public void StopRecording()
    {
        recording = false;
    }
}