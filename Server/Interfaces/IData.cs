namespace Interfaces;

public interface IData
{
    Task SendDataToClients(string data);
}