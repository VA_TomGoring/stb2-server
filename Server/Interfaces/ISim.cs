using Models;

namespace Interfaces;

public interface ISim
{
    public void Start();
    public void Stop();
    public IList<string> GetParameterNames();
    public IList<string> GetSignalNames();
    public void SetParam(string param, dynamic raw_value);
    public dynamic GetParam(string param);
    public IList<string> SelectedSignals { get; set; }
    public void StartRecording();
    public void StopRecording();
    public Dictionary<string, IList<DataPoint>> FetchRecordedData();
}