using Microsoft.AspNetCore.SignalR;

using Interfaces;

namespace Hubs;

public class DataHub : Hub<IData>
{
    public async Task SendDataToClients(string message)
    {
        await Clients.All.SendDataToClients(message);
    }
}