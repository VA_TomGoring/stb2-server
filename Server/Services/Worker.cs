namespace TestWorker;

using Microsoft.AspNetCore.SignalR;
using System.Text.Json;

using Hubs;
using Interfaces;

public class TimedHostedService : IHostedService
{
    private readonly ILogger<TimedHostedService> logger;
    private readonly IHubContext<DataHub, IData> dataHub;
    private readonly ISim simulation;
    private Timer timer = null!;

    public TimedHostedService(ILogger<TimedHostedService> logger, IHubContext<DataHub, IData> dataHub, ISim simulation)
    {
        this.logger = logger;
        this.dataHub = dataHub;
        this.simulation = simulation;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("Running...");
        timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(1));
        return Task.CompletedTask;
    }

    private async void DoWork(object? state)
    {
        var data = simulation.FetchRecordedData();
        var json = JsonSerializer.Serialize(data);
        await dataHub.Clients.All.SendDataToClients(json);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        logger.LogInformation("Stopping...");
        timer?.Change(Timeout.Infinite, 0);
        return Task.CompletedTask;
    }
}