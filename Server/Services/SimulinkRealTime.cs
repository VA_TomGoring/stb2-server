using MathWorks.ASAM.XIL.Server;
using ASAM.XIL.Implementation.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer;
using ASAM.XIL.Interfaces.Testbench.MAPort;
using ASAM.XIL.Interfaces.Testbench.ECUCPort;
using ASAM.XIL.Interfaces.Testbench.ECUMPort;
using ASAM.XIL.Interfaces.Testbench.Common.ValueContainer.Enum;
using ASAM.XIL.Interfaces.Testbench.Common.Capturing;

using Interfaces;
using Models;

namespace Simulink;

public class SimulinkRealTime : ISim
{
    public Testbench testbench;
    public IMAPort MAPort;
    public IECUCPort ECUCPort;
    public IECUMPort ECUMPort;
    public ILogger logger;
    public ICapture? capture;
    public int numCaptures;

    public SimulinkRealTime(string ConfigFilepath, ILogger logger)
    {
        this.logger = logger;
        logger.LogInformation("Initialising test bench...");
        testbench = new Testbench();
        MAPort = CreateMAPort(ConfigFilepath);
        ECUMPort = CreateECUMPort(ConfigFilepath);
        ECUCPort = CreateECUCPort(ConfigFilepath);
        logger.LogInformation("Setup done!");
    }

    private IMAPort CreateMAPort(string ConfigFilepath)
    {
        logger.LogInformation("Creating MAPort...");
        IMAPortFactory MAPortFactory = testbench.MAPortFactory;
        var MAPort = MAPortFactory.CreateMAPort("myMAPort");
        logger.LogInformation("Loading config to MAPort...");
        IMAPortConfig portConfig = MAPort.LoadConfiguration(ConfigFilepath);
        logger.LogInformation("Configuring MAPort...");
        MAPort.Configure(portConfig, true);
        logger.LogInformation("MAPort configured.");
        return MAPort;
    }

    private IECUMPort CreateECUMPort(string ConfigFilepath)
    {
        logger.LogInformation("Creating ECUMPort...");
        IECUMPortFactory ECUMPortFactory = testbench.ECUMPortFactory;
        var ECUMPort = ECUMPortFactory.CreateECUMPort("myECUMPort");
        logger.LogInformation("Loading ECUMPort config...");
        IECUMPortConfig PortConfig = ECUMPort.LoadConfiguration(ConfigFilepath);
        logger.LogInformation("Configuring ECUMPort...");
        ECUMPort.Configure(PortConfig);
        return ECUMPort;
    }

    private IECUCPort CreateECUCPort(string ConfigFilepath)
    {
        logger.LogInformation("Creating ECUCPort...");
        IECUCPortFactory eCUCPortFactory = testbench.ECUCPortFactory;
        var ECUCPort = eCUCPortFactory.CreateECUCPort("myECUCPort");
        logger.LogInformation("Loading ECUCPort config...");
        IECUCPortConfig ECUCPortConfig = ECUCPort.LoadConfiguration(ConfigFilepath);
        logger.LogInformation("Configuring ECUCPort...");
        ECUCPort.Configure(ECUCPortConfig);
        logger.LogInformation("Switching ECUCPort to WorkPage...");
        ECUCPort.SwitchToWorkPage();
        return ECUCPort;
    }

    public void Start()
    {
        logger.LogInformation("Starting sim...");
        MAPort.StartSimulation();
        logger.LogInformation("Sim started.");
    }

    public void Stop()
    {
        logger.LogInformation("Stopping sim");
        MAPort.StopSimulation();
        logger.LogInformation("Sim stopped");
    }

    public IList<string> GetParameterNames()
    {
        return ECUCPort!.VariableNames;
    }

    public IList<string> GetSignalNames()
    {
        return MAPort!.VariableNames;
    }

    public void SetParam(string param, dynamic raw_value)
    {
        var names = ECUCPort.CheckVariableNames(new List<string> { param });
        foreach (var name in names)
        {
            logger.LogInformation("{name}", name);
        }
        var type = typeof(GenericScalarValue<>).MakeGenericType(raw_value.GetType());
        var value = Activator.CreateInstance(type, raw_value).Value;
        ECUCPort.Write(param, value);
        logger.LogInformation("Param '{param}' set to '{value}'", param, (string)value.Value.ToString());
    }

    public dynamic GetParam(string param)
    {
        var value = ExtractValue(ECUCPort.Read(param));
        logger.LogInformation("Param '{param}' has value of '{value}'", param, (string)value.Value.ToString());
        return value.Value;
    }

    public dynamic ExtractValue(IBaseValue raw_value)
    {
        switch (raw_value.Type)
        {
            case DataType.eBOOLEAN:
                return ((BooleanValue)raw_value).Value;
            case DataType.eFLOAT:
                return ((FloatValue)raw_value).Value;
            case DataType.eINT:
                return ((IntValue)raw_value).Value;
            case DataType.eSTRING:
                return ((StringValue)raw_value).Value;
            case DataType.eUINT:
                return ((UintValue)raw_value).Value;
            default:
                throw new NotImplementedException($"Unimplemented data type - '{raw_value.Type}'");
        }
    }

    public void CreateCapture(string taskName)
    {
        capture = ECUMPort.CreateCapture(taskName);
    }

    public IList<string> SelectedSignals
    {
        get => capture!.Variables;
        set => capture!.Variables = value;
    }

    public void StartRecording()
    {
        ICapturingFactory capturingFactory = testbench.CapturingFactory;
        ICaptureResultMemoryWriter writer = capturingFactory.CreateCaptureResultMemoryWriter();
        capture!.Start(writer);
    }

    public void StopRecording()
    {
        capture!.Stop();
    }

    public Dictionary<string, IList<DataPoint>> FetchRecordedData()
    {
        var capturedData = capture!.Fetch(false);
        var signalGroupData = capturedData.GetSignalGroupValue("BaseRate");

        var data = new Dictionary<string, IList<DataPoint>>();
        var variableValueLists = signalGroupData.YVectors;

        for (int i = 0; i < signalGroupData.VariableNames.Count; i++)
        {
            var variableName = signalGroupData.VariableNames[i];
            var variableValues = ((FloatVectorValue)variableValueLists[i]).Value;
            var variableData = new List<DataPoint>();
            for (int j = 0; j < signalGroupData.XVector.Count; j++)
            {
                var timestamp = ((FloatVectorValue)signalGroupData.XVector).Value[j];
                var variableValue = variableValues[j];
                variableData.Add(new DataPoint(timestamp, variableValue));
                logger.LogInformation("Logged {numCaptures} samples so far", numCaptures);
                numCaptures++;
            }
            data.Add(variableName, variableData);
        }
        logger.LogInformation("Logged {numCaptures} samples so far", numCaptures);
        return data;
    }
}