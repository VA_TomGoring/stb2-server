using System.Text.Json;
using Hubs;
using TestWorker;
using Interfaces;
using Testing;

// using System.Net.Http.Json;

class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddSignalR();

        builder.Services.AddCors(options =>
        {
            options.AddPolicy("ClientPermission", policy =>
            {
                policy.AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins("http://localhost:3000")
                    .AllowCredentials();
            });
        });

        builder.Services.AddSingleton<ISim, TestSim>();

        builder.Services.AddHostedService<TimedHostedService>();

        WebApplication app = builder.Build();

        app.UseCors("ClientPermission");

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.MapGet("/signals", (ISim sim) =>
        {
            return JsonSerializer.Serialize(sim.GetSignalNames());
        });

        app.MapPost("/signals/startrecording", (ISim sim) =>
        {
            sim.StartRecording();
        });

        app.MapPost("/signals/stoprecording", (ISim sim) =>
        {
            sim.StopRecording();
        });

        app.MapPost("/signals/setsignals", async context =>
        {
            var sim = context.RequestServices.GetService<ISim>();
            var body = await context.Request.ReadFromJsonAsync<List<string>>();
            if (body != null)
            {
                sim!.SelectedSignals = body;
            }
        });

        app.MapGet("/signals/setsignals", (ISim sim) =>
        {
            return JsonSerializer.Serialize(sim.SelectedSignals);
        });

        app.MapHub<DataHub>("/hubs/data");

        app.Run();
    }
}
